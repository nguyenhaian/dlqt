import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { GioiThieuComponent } from './gioi-thieu/gioi-thieu.component';

import { HomeModule } from './home/home.module'

@NgModule({
    declarations: [
        AppComponent,
        GioiThieuComponent
    ],
    imports: [
        BrowserModule,
        HomeModule,
        AppRoutingModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
