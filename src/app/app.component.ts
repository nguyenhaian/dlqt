import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';
    selfurl = '';
    datenow = new Date();
    private sub: any;

    constructor(private route: Router) {
        route.events.subscribe((url: any) => {
            // console.log("AppComponent path", route.url)
            this.selfurl = route.url;
        });
    }

    checkactive() {
        return 1;
    }
}
