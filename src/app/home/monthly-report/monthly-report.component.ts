import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-monthly-report',
    templateUrl: './monthly-report.component.html',
    styleUrls: ['./monthly-report.component.css']
})
export class MonthlyReportComponent implements OnInit, OnDestroy {
    ith: number;
    year = 2018;
    private sub: any;

    constructor(private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            // console.log("route.params", +params['ith'])
            // alert("ith: " + params['ith'])
            this.ith = +params['ith']; // (+) converts string 'id' to a number

            // In a real app: dispatch action to load the details here.
        });

    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    gotodetail(chiso) {
        console.log('gotodetail', chiso)
        this.router.navigate(['/detail', chiso, this.ith]);
    }

}
