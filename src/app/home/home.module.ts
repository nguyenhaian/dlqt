import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';

import { HomeRoutingModule } from './home-routing.module';
import { MonthlyReportComponent } from './monthly-report/monthly-report.component';
import { ListReportComponent } from './list-report/list-report.component';
import { DetailReportComponent } from './detail-report/detail-report.component';

@NgModule({
    imports: [
        CommonModule,
        HomeRoutingModule
    ],
    declarations: [HomeComponent, MonthlyReportComponent, ListReportComponent, DetailReportComponent]
})
export class HomeModule { }
