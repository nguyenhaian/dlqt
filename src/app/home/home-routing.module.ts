import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component'
import { MonthlyReportComponent } from './monthly-report/monthly-report.component'
import { ListReportComponent } from './list-report/list-report.component'
import { DetailReportComponent } from './detail-report/detail-report.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children:[
            {
                path:'',
                component: ListReportComponent,
            },
            {
                path: 'kqdot/:ith',
                component: MonthlyReportComponent,
            },
            {
                path: 'detail/:chiso/:ith',
                component: DetailReportComponent,
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }