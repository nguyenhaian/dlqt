import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-detail-report',
    templateUrl: './detail-report.component.html',
    styleUrls: ['./detail-report.component.css']
})
export class DetailReportComponent implements OnInit, OnDestroy {
    ith: number;
    chiso: string;
    year = 2018;
    err: any = "";
    img1 = "";
    img3 = "";
    img2 = "";
    title = "";
    showimg2 = false;
    private sub: any;

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.ith = +params['ith']; // (+) converts string 'id' to a number
            this.chiso = params['chiso']; // (+) converts string 'id' to a number

            console.log("DetailReportComponent route param", this.chiso, this.ith)

            this.err = "";
            if (this.ith != 9) { this.err = "Chưa có dữ liệu" }

            switch (this.chiso) {
                case 'AQI': {
                    this.title = `BẢN ĐỒ AQI MÔI TRƯỜNG KHÔNG KHÍ THÀNH PHỐ ĐÀ NẴNG THÁNG ${this.ith}-${this.year}`;
                    this.img1 = "assets/img/AQI/aqit92018.jpg";
                } break;
                case 'NO2':
                    this.title = `BẢN ĐỒ MÔI TRƯỜNG THÀNH PHỐ ĐÀ NẴNG CHỈ TIÊU NO2 TRONG KHÔNG KHÍ THÁNG ${this.ith}-${this.year}`;
                    this.img1 = "assets/img/NO2/NO2T92018.jpg";
                    this.img2 = "assets/img/NO2/no2 t92018.jpg";
                    break;
                case 'O3':
                    this.title = `BẢN ĐỒ O3 MÔI TRƯỜNG KHÔNG KHÍ THÀNH PHỐ ĐÀ NẴNG THÁNG ${this.ith}-${this.year}`;
                    this.img1 = "assets/img/O3/O3T92018.jpg";
                    this.img2 = "assets/img/O3/o3 t92018.jpg";
                    break;
                case 'PM10':
                    this.title = `BẢN ĐỒ PM10 MÔI TRƯỜNG KHÔNG KHÍ THÀNH PHỐ ĐÀ NẴNG THÁNG ${this.ith}-${this.year}`;
                    this.img1 = "assets/img/PM10/PM10T92018.jpg";
                    this.img2 = "assets/img/PM10/pm10 t92018.jpg";
                    break;
                case 'SO2':
                    this.title = `BẢN ĐỒ SO2 MÔI TRƯỜNG KHÔNG KHÍ THÀNH PHỐ ĐÀ NẴNG THÁNG ${this.ith}-${this.year}`;
                    this.img1 = "assets/img/SO2/SO2T92018.jpg";
                    this.img2 = "assets/img/SO2/so2 t92018.jpg";
                    break;
                case 'TSP':
                    this.title = `BẢN ĐỒ TSP MÔI TRƯỜNG KHÔNG KHÍ THÀNH PHỐ ĐÀ NẴNG THÁNG ${this.ith}-${this.year}`;
                    this.img1 = "assets/img/TSP/TSPT92018.jpg";
                    this.img2 = "assets/img/TSP/tsp t92018.jpg";
                    break;
                case 'WQI':
                    this.title = `KẾT QUẢ QUAN TRẮC MÔI TRƯỜNG NƯỚC SÔNG THEO WQI CỦA THÁNG ${this.ith}-${this.year}`;
                    this.img1 = "assets/img/WQI/chi tiet t9.jpg";
                    this.img2 = "assets/img/WQI/chu thich t9.jpg";
                    this.img3 = "assets/img/WQI/WQI tháng 9.jpg";
                    break;

                default:
                    this.err = "Chưa có dữ liệu"
                    break;
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    viewfullscreen1(img1) {

    }
}
