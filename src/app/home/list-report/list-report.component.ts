import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-list-report',
    templateUrl: './list-report.component.html',
    styleUrls: ['./list-report.component.css']
})
export class ListReportComponent implements OnInit {

    constructor(private router: Router) { }

    ngOnInit() {
    }

    showAlert() {
        alert("Dữ liệu đang cập nhật!");
    }

    goto(ith) {
        console.log('goto', ith)
        this.router.navigate(['/kqdot', ith]);
    }
}
