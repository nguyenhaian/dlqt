import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { GioiThieuComponent } from './gioi-thieu/gioi-thieu.component';

const appRoutes: Routes = [
    // { path: '', redirectTo: '/home', pathMatch: 'full' },
    // { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'gioi-thieu', component: GioiThieuComponent },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
